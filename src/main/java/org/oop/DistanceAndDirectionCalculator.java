package org.oop;

interface DistanceAndDirectionInterface {

    public double distance(Point from, Point to);

    public double direction(Point from, Point to);
}

public class DistanceAndDirectionCalculator implements DistanceAndDirectionInterface {

    private double findCoOrdinateDifference(double coOrdinate1, double coOrdinate2) {
        return coOrdinate1 - coOrdinate2;
    }

    @Override
    public double distance(Point from, Point to) {
        double xDistance = findCoOrdinateDifference(to.getX(), from.getX());
        double yDistance = findCoOrdinateDifference(to.getY(), from.getY());
        return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2));
    }

    @Override
    public double direction(Point from, Point to) {
        double xDistance = findCoOrdinateDifference(to.getX(), from.getX());
        double yDistance = findCoOrdinateDifference(to.getY(), from.getY());
        return Math.atan2(yDistance, xDistance);
    }
}
